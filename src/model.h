#include "linear_algebra.h"
#include "quaternion.h"

#ifndef MODEL_H
#define MODEL_H

struct Model
{
    Vec3 position;
    Quaternion orientation;
    Mat4 matrix;
};

void model_init(struct Model* model);
void model_move(struct Model* model, Vec3 translation);
void model_rotate_q(struct Model* model, Quaternion q);
void model_rotate(struct Model* model, Vec3 axis, float angle);
void model_update(struct Model* model);
void model_update_from_matrix(struct Model* model);
void model_scale(struct Model* model, float scale);

int model_is_valid(struct Model* model);

#endif
