#ifndef TREE_H
#define TREE_H

#include "renderable.h"

Renderable* tree_new(GLuint shader);

#endif
