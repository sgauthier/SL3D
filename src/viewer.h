#include "renderable.h"

#ifndef VIEWER_H
#define VIEWER_H

typedef struct Viewer Viewer;

Viewer* viewer_new(unsigned int width, unsigned int height, const char* title);
void viewer_free(Viewer* viewer);

void viewer_set_callbacks(Viewer* viewer, void (*cursor_callback)(double, double, double, double, int, int, int, void*), void (*wheel_callback)(double, double, void*), void (*key_callback)(int, int, int, int, void*), void (*close_callback)(void*), void* userData);

void viewer_process_events(Viewer* viewer);
void viewer_render(Viewer* viewer);
int viewer_add_renderable(Viewer* viewer, struct Renderable* renderable);
int viewer_add_directionnal_light(Viewer* viewer, struct DirectionnalLight* light);
int viewer_add_local_light(Viewer* viewer, struct LocalLight* light);

struct Camera* viewer_get_camera(Viewer* viewer);
int viewer_get_width(Viewer* viewer);
int viewer_get_height(Viewer* viewer);
void viewer_fps(Viewer* viewer);

void viewer_print_renderables(Viewer* viewer);

#endif
