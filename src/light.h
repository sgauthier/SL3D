#ifndef LIGHT_H
#define LIGHT_H

struct DirectionnalLight
{
    Vec3 direction;

    Vec3 ambient;
    Vec3 diffuse;
    Vec3 specular;
};

struct LocalLight
{
    Vec3 position;
    float intensity;

    Vec3 ambient;
    Vec3 diffuse;
    Vec3 specular;
};


#endif
