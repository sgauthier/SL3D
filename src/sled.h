#include "renderable.h"
#include "map.h"
#include "linear_algebra.h"
#include "quaternion.h"

#ifndef SLED_H
#define SLED_H

enum SledUpdateError
{
    SLED_NO_ERROR,
    SLED_OUT_OF_MAP,
    SLED_TOO_MUCH_ROTATION,
    SLED_COLLISION_WITH_TREE
};

struct Sled
{
    Renderable* master;
    Vec3 speed;
    Quaternion angularSpeed;
    Vec3 traction;
    float brake;
    
    int isFlying, isGliding;
};


struct Sled* sled_new(void);
void sled_free(struct Sled* sled);

Renderable* sled_get_renderable(struct Sled* sled);
struct Model* sled_get_model(struct Sled* sled);

enum SledUpdateError sled_update(struct Sled* sled, struct Map* map, float step);

#endif
