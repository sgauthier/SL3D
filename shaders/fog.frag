#version 130 
#

struct Material
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct DirectionnalLight
{
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct LocalLight
{
    vec3 position;
    float intensity;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec2 coordTexture;
in vec3 surfelPosition;
in vec3 surfelNormal;
in vec4 color;
in float pixDistance;

uniform sampler2D tex;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 cameraPosition;

uniform int nbDirectionnalLights;
uniform int nbLocalLights;
uniform DirectionnalLight directionnalLights[10];
uniform LocalLight localLights[10];

uniform Material material;

out vec4 out_Color;


vec3 compute_directionnal_light(DirectionnalLight light, vec3 surfelToCamera)
{
    vec3 surfelToLight = -light.direction;

    float diffuseFactor = max(dot(surfelToLight, surfelNormal), 0.0);
    vec3 reflectDirection = reflect(surfelToLight, surfelNormal);
    float specularFactor = pow(max(dot(surfelToCamera, reflectDirection), 0.0), material.shininess);

    vec3 ambient = light.ambient * material.ambient;
    vec3 diffuse = diffuseFactor * light.diffuse * material.diffuse;
    vec3 specular = 0*specularFactor * light.specular * material.specular;

    return (ambient + diffuse + specular);
}

vec3 compute_local_light(LocalLight light, vec3 surfelToCamera)
{
    // Diffuse shading
    vec3 surfelToLight = light.position - surfelPosition;
    float distance = length(surfelToLight);
    surfelToLight *= float(1) / distance;     // normalization
    float diffuseFactor = max(dot(surfelNormal, surfelToLight), 0.0);

    // Specular shading
    vec3 reflectDirection = reflect(-surfelToLight, surfelNormal);
    float specularFactor = pow(max(dot(surfelToCamera, reflectDirection), 0.0), material.shininess);

    // Attenuation: TODO
    //float attenuation = 1.0;
    float attenuation = light.intensity / (distance);

    // Combine results
    vec3 ambient  = attenuation *                   light.ambient  * material.ambient ;
    vec3 diffuse  = attenuation * diffuseFactor  * light.diffuse  * material.diffuse ;
    vec3 specular = 0*attenuation * specularFactor * light.specular * material.specular;

    return (ambient + diffuse + specular);
}

vec3 compute_local_light(LocalLight light, vec3 surfelToCamera, vec3 surfelPosition, Material material)
{
    return vec3(0.0, 0.0, 0.0);
}

vec4 compute_fog(vec4 color, float distance)
{
    vec4 fogColor = vec4(0.7, 0.7, 0.8, 1.0);
    float opaqueDistance = 200;

    if (distance > opaqueDistance)
        return fogColor;
    else
    {
        float u = distance/opaqueDistance;
        return vec4((1-u)*vec3(color) + u*vec3(fogColor), color[3]);
    }
}

void main()
{
    vec3 lightFactor = vec3(0.0, 0.0, 0.0);;
    vec3 surfelToCamera = normalize(cameraPosition - surfelPosition);
    int i;

    for (i = 0; i < nbDirectionnalLights; i++)
    {
        lightFactor += compute_directionnal_light(directionnalLights[i], surfelToCamera);
    }

    for (i = 0; i < nbLocalLights; i++)
    {
        lightFactor += compute_local_light(localLights[i], surfelToCamera);
    }

    out_Color = texture(tex, coordTexture);
    if(coordTexture == vec2(0,0)) // No texture
        out_Color = color;

    out_Color *= vec4(lightFactor, 1.0);
    out_Color = compute_fog(out_Color, pixDistance);
}
