#include <stdio.h>

#include "flag.h"
#include "renderables/mesh.h"
#include "resources.h"
#include "rand_tools.h"

Renderable* flag_new(Map* map, GLuint shader)
{
    Renderable* res = mesh_new("models", "flag.obj", shader, textures[TEXTURE_FLAG]);
    Vec3 flagPos;
    flagPos[0] = rand_float(0, map->size * map->unitSize);
    flagPos[2] = rand_float(0, map->size * map->unitSize);
    flagPos[1] = map_get_height(map, flagPos[0], flagPos[2]);

    printf("Flag at position: %f - %f\n", flagPos[0], flagPos[2]);

    renderable_move(res, flagPos);

    return res;
}
