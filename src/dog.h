#ifndef DOG_H
#define DOG_H

#include "renderable.h"
#include "animation.h"
#include "map.h"
#include "sled.h"

enum DogUpdateError
{
    DOG_NO_ERROR,
    DOG_SLED_OUT_OF_MAP = SLED_OUT_OF_MAP,
    DOG_SLED_TOO_MUCH_ROTATION = SLED_TOO_MUCH_ROTATION,
    DOG_SLED_COLLISION_WITH_TREE = SLED_COLLISION_WITH_TREE,
    DOG_OUT_OF_MAP,
    DOG_COLLISION_WITH_TREE,
    DOG_COLLISION_WITH_SLED
};

enum PullType
{
    PULL_NONE,
    PULL_DOG,
    PULL_SLED
};

enum DogStatus
{
    DOG_IDLE = 0,
    DOG_WALKING,
    DOG_RUNNING,
    DOG_NB_STATUS
};

typedef struct Dog Dog;

Dog* dog_new(enum PullType pullType, void* pulledObject);

void dog_incr_force(Dog* dog);
void dog_decr_force(Dog* dog);
void dog_rotate(Dog* dog, float angle);

enum DogUpdateError dog_update(Dog* dog, struct Map* map, float dt);

Renderable* dog_get_renderable(Dog* dog);
struct Model* dog_get_model(Dog* dog);

void dog_free(Dog* dog);

#endif

