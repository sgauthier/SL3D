#include "renderable.h"

#ifndef MAP_H
#define MAP_H

#include "renderable.h"

typedef struct Map
{
    unsigned int size, nbCellSize;
    float unitSize;
    float* height;

    Renderable* master;
    Renderable** cells;
    Renderable* minimap;
} Map;

struct Map* map_new(unsigned int size, unsigned int cellSize, float unitSize,
                    float roughness, GLuint shader);
void map_free(struct Map* map);

float map_get_height(struct Map* map, float x, float y);
int map_get_normal(struct Map* map, float x, float y, Vec3 normal);

#endif
