#include <GL/glew.h>
#include "../renderable.h"

#ifndef CUBE_H
#define CUBE_H

Renderable* cube_new(float size, GLuint shader, GLuint texture);

#endif
