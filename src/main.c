#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "game.h"

void print_help(char* command)
{
    printf("%s [-f] [-fd <float>] [-fc <float>] [-tr <float>] [-h]\n"
           "    -f: activates fog (default: off)\n"
           "    -fd: forest density (default: 0.92)\n"
           "    -fc: forest compactness (default: 6.0)\n"
           "    -tr: terrain roughness (default: 0.4)\n"
           "    -h: prints this help\n", command);
}

int parse_args(int argc, char** argv, struct Params* params)
{
    int i;

    params->fog = 0;
    params->forestDensity = 0.92;
    params->forestCompactness = 6.0;
    params->terrainRoughness = 0.4;

    for (i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "-f"))
            params->fog = 1;
        else if (!strcmp(argv[i], "-fd") && i+1 < argc)
            params->forestDensity = strtof(argv[++i], NULL);
        else if (!strcmp(argv[i], "-fc") && i+1 < argc)
            params->forestCompactness = strtof(argv[++i], NULL);
        else if (!strcmp(argv[i], "-tr") && i+1 < argc)
            params->terrainRoughness = strtof(argv[++i], NULL);
        else
        {
            print_help(argv[0]);
            return 0;
        }
    }

    return 1;
}

int main(int argc, char** argv)
{
    Game* game;
    struct Params params;

    if (parse_args(argc, argv, &params))
    {
        srand(time(NULL));

        if ((game = game_new(&params)))
            while (game_is_running(game))
                game_update(game);

        game_free(game);
    }

    return 0;
}
