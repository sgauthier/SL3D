#version 130

in vec3 in_Vertex;
in vec2 in_TexCoord;
in vec3 in_Normal;

uniform mat3 NIT = mat3(1.0);
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 solidColor;

out vec2 coordTexture;
out vec3 surfelPosition;
out vec3 surfelNormal;
out vec4 color;

void main()
{
    gl_Position = projection * view * model * vec4(in_Vertex, 1.0);
    color = vec4(solidColor, 1.0);

    coordTexture = in_TexCoord;
    surfelPosition = vec3(model * vec4(in_Vertex, 1.0));
    surfelNormal = normalize(NIT * in_Normal);
}
