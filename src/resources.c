#include <GL/glew.h>

#include "resources.h"
#include "shader.h"
#include "texture.h"
#include "material.h"

GLuint shaders[NB_SHADERS] = {0};
GLuint textures[NB_TEXTURES] = {0};

static const char *const vertexShaders[] =
{
    "shaders/solidColor.vert",
    "shaders/solidTexture.vert",
    "shaders/basic.vert",
    "shaders/fog.vert",
    "shaders/minimap.vert"
};

static const char *const fragmentShaders[] =
{
    "shaders/solidColor.frag",
    "shaders/solidTexture.frag",
    "shaders/basic.frag",
    "shaders/fog.frag",
    "shaders/minimap.frag"
};

static const char *const texturePaths[] =
{
    "textures/tux.png",
    "textures/tux_rgba.png",
    "textures/skybox.png",
    "textures/snow.png",
    "textures/dog.png",
    "textures/pine.png",
    "textures/flag.png",
    "textures/sled.png"
};

static const struct Material defaultMaterialDef =
{
    {1.0, 1.0, 1.0},
    {1.0, 1.0, 1.0},
    {1.0, 1.0, 1.0},
    1.0
};

const struct Material *const defaultMaterial = &defaultMaterialDef;


int resources_init(void)
{
    unsigned int i;

    for (i = 0; i < NB_SHADERS; i++)
    {
        if (!(shaders[i] = shader_compile(vertexShaders[i], fragmentShaders[i])))
        {
            resources_free();
            return 0;
        }
    }

    for (i = 0; i < NB_TEXTURES; i++)
    {
        if (!(textures[i] = texture_load_from_file(texturePaths[i])))
        {
            resources_free();
            return 0;
        }
    }

    return 1;
}

void resources_free(void)
{
    unsigned int i;
    
    for (i = 0; i < NB_SHADERS; i++)
    {
        if (shaders[i])
            glDeleteProgram(shaders[i]);
        else
            break;
    }

    glDeleteTextures(NB_TEXTURES, textures);
}
