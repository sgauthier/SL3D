#include "../renderable.h"
#include "../map.h"

#ifndef MINIMAP_H
#define MINIMAP_H

Renderable* minimap_new(float* heightMap, unsigned int size);
Renderable* minimap_add_point(Renderable* minimap, float r, float g, float b);
void minimap_move_point(struct Map* map, Renderable* point, float x, float z);

#endif
