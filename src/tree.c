#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#include "tree.h"
#include "resources.h"
#include "renderables/mesh.h"

Renderable* tree_new(GLuint shader)
{
    Renderable *tree;
    Mesh treeMesh, treeRedu;

    mesh_load(&treeMesh, "models", "tree.obj");
    tree = mesh_new_from_mesh(&treeMesh, shader, textures[TEXTURE_TREE]);

    /*Reduced model auto-generation*/
    {
        float xmin = FLT_MAX, xmax = -FLT_MAX, ymin = FLT_MAX, ymax = -FLT_MAX;
        int i;

        for (i = 0; i < treeMesh.numVertices; i++)
        {
            xmin = treeMesh.vertices[3*i] < xmin ? treeMesh.vertices[3*i] : xmin;
            xmax = treeMesh.vertices[3*i] > xmax ? treeMesh.vertices[3*i] : xmax;
            ymin = treeMesh.vertices[3*i + 1] < ymin ? treeMesh.vertices[3*i + 1] : ymin;
            ymax = treeMesh.vertices[3*i + 1] > ymax ? treeMesh.vertices[3*i + 1] : ymax;
        }

        treeRedu.numVertices = 6;
        treeRedu.vertices = malloc(3 * 6 * sizeof(float));
        treeRedu.normals = malloc(3 * 6 * sizeof(float));
        treeRedu.texCoords = malloc(2 * 6 * sizeof(float));

        treeRedu.vertices[0] = 0; treeRedu.vertices[1] = ymax; treeRedu.vertices[2] = 0;
        treeRedu.vertices[3] = xmin; treeRedu.vertices[4] = ymin; treeRedu.vertices[5] = 0;
        treeRedu.vertices[6] = xmax; treeRedu.vertices[7] = ymin; treeRedu.vertices[8] = 0;
        treeRedu.vertices[9] = 0; treeRedu.vertices[10] = ymax; treeRedu.vertices[11] = 0;
        treeRedu.vertices[12] = 0; treeRedu.vertices[13] = ymin; treeRedu.vertices[14] = xmin;
        treeRedu.vertices[15] = 0; treeRedu.vertices[16] = ymin; treeRedu.vertices[17] = xmax;

        treeRedu.normals[0] = 0; treeRedu.normals[1] = 0; treeRedu.normals[2] = 1;
        treeRedu.normals[3] = 0; treeRedu.normals[4] = 0; treeRedu.normals[5] = 1;
        treeRedu.normals[6] = 0; treeRedu.normals[7] = 0; treeRedu.normals[8] = 1;
        treeRedu.normals[9] = 1; treeRedu.normals[10] = 0; treeRedu.normals[11] = 0;
        treeRedu.normals[12] = 1; treeRedu.normals[13] = 0; treeRedu.normals[14] = 0;
        treeRedu.normals[15] = 1; treeRedu.normals[16] = 0; treeRedu.normals[17] = 0;

        treeRedu.texCoords[0] = 0.856; treeRedu.texCoords[1] = 0.045;
        treeRedu.texCoords[2] = 0.712; treeRedu.texCoords[3] = 0.973;
        treeRedu.texCoords[4] = 1; treeRedu.texCoords[5] = 1;
        treeRedu.texCoords[6] = 0.856; treeRedu.texCoords[7] = 0.045;
        treeRedu.texCoords[8] = 0.712; treeRedu.texCoords[9] = 0.973;
        treeRedu.texCoords[10] = 1; treeRedu.texCoords[11] = 1;
    }

    /*Loading reduced models in tree renderable*/
    renderable_set_reduced_model(tree, FAR, treeRedu.numVertices, treeRedu.vertices, treeRedu.normals, treeRedu.texCoords);
    renderable_set_reduced_model(tree, INF, 0, NULL, NULL, NULL);
    renderable_set_solid_color(tree, 0.3, 0.2, 0);

    return tree;
}
