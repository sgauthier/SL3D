#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#include "map.h"
#include "renderable.h"
#include "resources.h"
#include "linear_algebra.h"
#include "rand_tools.h"
#include "renderables/minimap.h"

static void set_cell_data(unsigned int x, unsigned int y, unsigned int mapSize, float unitSize, float* height, float* vertex, float* normal)
{
    unsigned int center = y * mapSize + x;
    unsigned int yPrev = (y - 1) * mapSize + x;
    unsigned int yNext = (y + 1) * mapSize + x;
    float xPrevHeight = 0, xNextHeight = 0, yPrevHeight = 0, yNextHeight = 0;

    vertex[0] = ((float)x) * unitSize;
    vertex[2] = ((float)y) * unitSize;
    vertex[1] = height[center];

    xPrevHeight = x ? height[center - 1] : (2 * height[center] - height[center + 1]);
    xNextHeight = (x < mapSize - 1) ? height[center + 1] : (2 * height[center] - height[center - 1]);
    yPrevHeight = y ? height[yPrev] : (2 * height[center] - height[yNext]);
    yNextHeight = (y < mapSize - 1) ? height[yNext] : (2 * height[center] - height[yPrev]);
    
    normal[2] = xNextHeight - xPrevHeight;
    normal[0] = yNextHeight - yPrevHeight;
    normal[1] = 2;
    normalize3(normal);
}

struct Map* map_new(unsigned int size, unsigned int cellSize, float unitSize, float roughness, GLuint shader)
{
    struct Map* map;
    int error = 0;
    unsigned int x = 0, y = 0, cx, cy;
    unsigned int nVert = 6 * cellSize * cellSize, nbCellSize = size / cellSize;
    float* vertices;
    float* normals;
    float* texCoords;
    Renderable* cell;

    printf("Generating map...\n");

    vertices = malloc(3 * nVert * sizeof(float));
    normals = malloc(3 * nVert * sizeof(float));
    texCoords = malloc(2 * nVert * sizeof(float));

    if (vertices && normals && (map = malloc(sizeof(struct Map))))
    {
        map->size = size;
        map->nbCellSize = nbCellSize;
        map->unitSize = unitSize;
        size++;

        if ((map->height = malloc(size * size * sizeof(float))))
        {
            /*
            for (y = 0; y < size; y++)
                for (x = 0; x < size; x++)
                    map->height[y * size + x] = x;
            */

            float* pondmap = malloc(size*size*sizeof(float));
            gen_perlin(pondmap, size, size/2, roughness, 1.5, 0);
            gen_diamond_square(map->height, pondmap, size);


            /* Create master renderable */
            if ((map->master = renderable_new(0, NULL, NULL, NULL, 0, 0)))
            {
                int a;

                renderable_set_name(map->master, "mapMaster");

                for (a = 0; a < nVert/6; a++)
                {
                    texCoords[12*a] = 0; texCoords[12*a+1] = 0;
                    texCoords[12*a+2] = 0; texCoords[12*a+3] = 1;
                    texCoords[12*a+4] = 1; texCoords[12*a+5] = 0;
                    texCoords[12*a+6] = 1; texCoords[12*a+7] = 0;
                    texCoords[12*a+8] = 0; texCoords[12*a+9] = 1;
                    texCoords[12*a+10] = 1; texCoords[12*a+11] = 1;
                }
                /* Create renderable cells */
                if ((map->cells = malloc(nbCellSize * nbCellSize * sizeof(Renderable*))))
                {
                    for (y = 0; y < map->size; y += cellSize)
                    {
                        for (x = 0; x < map->size; x += cellSize)
                        {
                            char rName[100];
                            for (cy = 0; cy < cellSize; cy++)
                            for (cx = 0; cx < cellSize; cx++)
                            {
                                unsigned int base = 6 * 3 * (cy * cellSize + cx);
                                set_cell_data(x + cx,     y + cy,     size, unitSize, map->height, vertices + base, normals + base);
                                set_cell_data(x + cx,     y + cy + 1, size, unitSize, map->height, vertices + base + 3, normals + base + 3);
                                set_cell_data(x + cx + 1, y + cy,     size, unitSize, map->height, vertices + base + 6, normals + base + 6);
                                set_cell_data(x + cx + 1, y + cy,     size, unitSize, map->height, vertices + base + 9, normals + base + 9);
                                set_cell_data(x + cx,     y + cy + 1, size, unitSize, map->height, vertices + base + 12, normals + base + 12);
                                set_cell_data(x + cx + 1, y + cy + 1, size, unitSize, map->height, vertices + base + 15, normals + base + 15);
                            }
                            error = (!(cell = renderable_new(nVert, vertices, normals, texCoords, shader, textures[TEXTURE_SNOW])))
                                 || (!renderable_add_child(map->master, cell));
                            if (error)
                            {
                                renderable_free(cell);
                                break;
                            }
                            renderable_set_solid_color(cell, 0.937, 0.933, 0.98);
                            renderable_set_material(cell, defaultMaterial);
                            renderable_set_reduced_model(cell, INF, 0, NULL, NULL, NULL);
                            sprintf(rName, "mapTile%d-%d", x, y);
                            renderable_set_name(cell, rName);
                            map->cells[(y / cellSize) * nbCellSize + (x / cellSize)] = cell;
                        }
                        if (error)
                            break;
                    }
                    if (x == map->size && y == map->size)
                    {
                        free(pondmap);
                        free(vertices);
                        free(normals);
                        free(texCoords);
                        map->minimap = NULL;

                        if ((map->minimap = minimap_new(map->height, map->size)))
                        {
                            if (renderable_add_child(map->master, map->minimap))
                                renderable_toggle_always_draw(map->minimap);
                            else
                                renderable_free(map->minimap);
                        }

                        return map;
                    }
                }
                renderable_free(map->master);
            }
            free(pondmap);
            free(map->height);
        }
        free(map);
    }

    fprintf(stderr, "Error: unable to allocate the map\n");
    free(vertices);
    free(normals);
    free(texCoords);

    printf("Done\n");

    return NULL;
}

void map_free(struct Map* map)
{
    if (map)
    {
        free(map->cells);
        renderable_free(map->master);
        free(map->height);
        free(map);
    }
}

float map_get_height(struct Map* map, float x, float y)
{
    unsigned int size = map->size + 1;
    unsigned int xBase = (int)(x /= map->unitSize);
    unsigned int yBase = (int)(y /= map->unitSize);
    float dx = x - ((float)xBase);
    float dy = y - ((float)yBase);
    float dzx, dzy, hBase;

    if (x <= 0 || x >= size || y <= 0 || y >= size)
        return FLT_MAX;

    if (dx + dy < 1.0)
    {
        hBase = map->height[yBase * size + xBase];
        dzx = map->height[yBase * size + xBase + 1] - hBase;
        dzy = map->height[(yBase + 1) * size + xBase] - hBase;
    }
    else
    {
        dx = 1 - dx;
        dy = 1 - dy;
        hBase = map->height[(yBase + 1) * size + xBase + 1];
        dzx = map->height[(yBase + 1) * size + xBase] - hBase;
        dzy = map->height[yBase * size + xBase + 1] - hBase;
    }

    return hBase + dx * dzx + dy * dzy;
}

int map_get_normal(struct Map* map, float x, float y, Vec3 normal)
{
    Vec3 u, v;
    unsigned int size = map->size + 1;
    unsigned int xBase = (int)(x /= map->unitSize);
    unsigned int yBase = (int)(y /= map->unitSize);
    float dx = x - ((float)xBase);
    float dy = y - ((float)yBase);

    if (x <= 0 || x >= size || y <= 0 || y >= size)
        return 0;

    if (dx + dy < 1.0)
    {
        u[0] = 1;
        u[2] = 0;
        u[1] = map->height[yBase * size + xBase + 1] - map->height[yBase * size + xBase];
        v[0] = 0;
        v[2] = 1;
        v[1] = map->height[(yBase + 1) * size + xBase] - map->height[yBase * size + xBase];
    }
    else
    {
        u[0] = -1;
        u[2] = 0;
        u[1] = map->height[(yBase + 1) * size + xBase] - map->height[(yBase + 1) * size + xBase + 1];
        v[0] = 0;
        v[2] = -1;
        v[1] = map->height[yBase * size + xBase + 1] - map->height[(yBase + 1) * size + xBase + 1];
    }
    cross3(normal, v, u);
    normalize3(normal);

    return 1;
}
