#include <stdio.h>
#include <stdlib.h>

#include "../renderable.h"
#include "../resources.h"
#include "../texture.h"
#include "../map.h"

static float vertices[] = {
    0.0, 0.0, 0.0,
    0.0, 0.2, 0.0,
    0.2, 0.0, 0.0,
    0.2, 0.0, 0.0,
    0.0, 0.2, 0.0,
    0.2, 0.2, 0.0
};

static float texCoords[] = {
    0.0, 0.0,
    1.0, 0.0,
    0.0, 1.0,
    0.0, 1.0,
    1.0, 0.0,
    1.0, 1.0
};

Renderable* minimap_new(float* heightMap, unsigned int size)
{
    GLuint texture = 0;
    unsigned char* buffer;
    unsigned char* ptr;
    unsigned int x, y;
    unsigned int size2 = size + 1;
    float cur, min = heightMap[0], max = heightMap[0];
    Vec3 t = {0.7, 0.7, 0.0};
    Renderable* ret = NULL;

    if (!(buffer = malloc(size * size * 3)))
    {
        fprintf(stderr, "Warning: minimap allocation failed\n");
        return NULL;
    }

    for (y = 0; y < size; y++)
    for (x = 0; x < size; x++)
    {
        cur = heightMap[y * size2 + x];
        if (cur > max)
            max = cur;
        else if (cur < min)
            min = cur;
    }

    ptr = buffer;
    for (y = 0; y < size; y++)
    for (x = 0; x < size; x++)
    {
        cur = (heightMap[y * size2 + x] - min) / (max - min);
        *ptr++ = 255.0 * cur;
        *ptr++ = 255.0 * cur;
        *ptr++ = 108.0 * cur;
    }

    texture = texture_load_from_buffer(buffer, size, size, 0);
    free(buffer);

    if ((ret = renderable_new(6, vertices, vertices, texCoords, shaders[SHADER_MINIMAP], texture)))
        renderable_move(ret, t);
    return ret;
}

Renderable* minimap_add_point(Renderable* minimap, float r, float g, float b)
{
    float vertices[18];
    Renderable* ret;

    vertices[0] = -0.01;
    vertices[1] = -0.01;
    vertices[2] = -0.1;
    vertices[3] = vertices[12] = 0.01;
    vertices[4] = vertices[13] = -0.01;
    vertices[5] = vertices[14] = -0.1;
    vertices[6] = vertices[9] = -0.01;
    vertices[7] = vertices[10] = 0.01;
    vertices[8] = vertices[11] = -0.1;
    vertices[15] = 0.01;
    vertices[16] = 0.01;
    vertices[17] = -0.1;

    if ((ret = renderable_new(6, vertices, vertices, NULL, shaders[SHADER_MINIMAP], 0)))
    {
        renderable_set_solid_color(ret, r, g, b);
        renderable_add_child(minimap, ret);
        renderable_toggle_always_draw(ret);
    }

    return ret;
}

void minimap_move_point(struct Map* map, Renderable* point, float x, float z)
{
    Vec3 t;
    model_init(renderable_get_model(point));
    t[0] = z / (map->size * map->unitSize * 5.0);
    t[1] = x / (map->size * map->unitSize * 5.0);
    t[2] = 0.0;
    model_move(renderable_get_model(point), t);
}
