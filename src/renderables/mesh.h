#include <GL/glew.h>
#include "../renderable.h"

#ifndef MESH_H
#define MESH_H

typedef struct Mesh
{
    struct Material mat;
    unsigned int numVertices;
    float* vertices;
    float* normals;
    float* texCoords;
} Mesh;

void mesh_load(struct Mesh* mesh, const char* modeldir, const char* filename);
void mesh_join(struct Mesh* dest, struct Mesh* source);
void mesh_translate(struct Mesh* mesh, Vec3 t);
void mesh_scale(struct Mesh* mesh, float scale);
void mesh_free(struct Mesh*);

Renderable* mesh_new(const char* modeldir, const char* filename, GLuint shader, GLuint texture);
Renderable* mesh_new_from_mesh(struct Mesh* mesh, GLuint shader, GLuint texture);

#endif
