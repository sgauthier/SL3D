DEPS := glfw3 glew libpng
BUILD := build
APP := SL3D

CFLAGS += -std=c89 -pedantic -march=native -Wall -D_XOPEN_SOURCE=500 -D_POSIX_C_SOURCE=200112L -O3 -DM_PI=3.14159265358979 $(shell pkg-config --cflags $(DEPS))
LDLIBS += -lm $(shell pkg-config --libs $(DEPS))
C_FILES := $(wildcard src/*.c src/renderables/*.c)
OBJECTS := $(addprefix $(BUILD)/,$(C_FILES:.c=.o))

.PHONY: all
all: check_deps $(BUILD)/$(APP)

$(BUILD)/$(APP): $(OBJECTS)
	@mkdir -p $(dir $@)
	$(CC) $(LDFLAGS) $(OBJECTS) $(LOADLIBES) $(LDLIBS) -o $@

$(BUILD)/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

.PHONY: check_deps
check_deps:
	@pkg-config --exists $(DEPS) || (echo "One of the following dependencies was not found: $(DEPS)"; false)

.PHONY: clean
clean:
	rm -rf $(BUILD)
