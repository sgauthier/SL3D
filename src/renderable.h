#include <GL/glew.h>
#include "camera.h"
#include "uniform.h"
#include "material.h"
#include "model.h"

#ifndef RENDERABLE_H
#define RENDERABLE_H

enum DistancesLevel
{
    NEAR = 0,
    FAR,
    INF,
    NB_DIST_LEVELS
};

typedef struct Renderable Renderable;

Renderable* renderable_new(unsigned int numVertices, float* vertices, float* normals,
                           float* texCoords, GLuint shader, GLuint texture);
Renderable* renderable_copy(Renderable* source);

void renderable_set_reduced_model(Renderable* renderable, enum DistancesLevel level,
                           unsigned int numVertices, float* vertices, float* normals, float* texCoords);

void renderable_free(Renderable* renderable);

void renderable_set_material(Renderable* renderable, const struct Material* material);
void renderable_set_solid_color(Renderable* renderable, float r, float g, float b);

int renderable_draw(Renderable* renderable, struct Uniforms* uniforms);
void renderable_display_bounding_box(Renderable* renderable);
void renderable_toggle_always_draw(Renderable* renderable);
int renderable_add_child(Renderable* parent, Renderable* child);
Renderable* renderable_get_child(Renderable* parent, int index);

void renderable_move(Renderable* renderable, Vec3 translation);
void renderable_rotate(Renderable* renderable, Vec3 axis, float angle);
void renderable_scale(Renderable* renderable, float scale);
void renderable_update_all_nits(struct Renderable* renderable);

void renderable_set_name(Renderable* renderable, char* name);
void print_renderable(Renderable* renderable);

struct Model* renderable_get_model(Renderable* renderable);
float distance_to_camera(const Renderable* r);

#endif
