#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <time.h>
#include <math.h>

#include "rand_tools.h"
#include "linear_algebra.h"

float rand_float(float min, float max)
{
    return ((float)rand())/((float)RAND_MAX) * (max - min) + min;
}

void rand_vec2(Vec2 v, float max_norm)
{
    v[0] = rand_float(-1.0,1.0);
    v[1] = rand_float(-1.0,1.0);

    /*normalize2(v);*/
    mul2sv(v, max_norm, v);
}

static float ease(float t)
{
    return t * t * t * (t * (t * 6 - 15) + 10);
}

static float lerp(float t, float a, float b)
{
    return a + t*(b-a);
}

void gen_perlin(float* array, int size, int resol, float amplitude, float exponent, int additive)
{
    int gradNb = size/resol + 1;
    Vec2* gradients = calloc(gradNb * gradNb, sizeof(Vec3));
    int i, j;
    float min = FLT_MAX;

    for (i = 0; i < gradNb * gradNb; i++)
    {
        rand_vec2(gradients[i], amplitude);
    }

    for (j = 0; j < size; j++)
    {
        for (i = 0; i < size; i++)
        {
            float u = ((float)(i%resol)/((float) resol));
            float v = ((float)((j%resol)))/((float) resol);
            float* gv1 = gradients[gradNb*(j/resol) + (i/resol)];
            float* gv2 = gradients[gradNb*(j/resol + 1) + (i/resol)];
            float* gv3 = gradients[gradNb*(j/resol + 1) + (i/resol + 1)];
            float* gv4 = gradients[gradNb*(j/resol) + (i/resol + 1)];
            float g1 = 0, g2 = 0, g3 = 0, g4 = 0;
            Vec2 dv1 = {0,0}, dv2 = {0,0}, dv3 = {0,0}, dv4 = {0,0};
            float value = 0.0;
            dv1[0] = u;         dv1[1] = v;
            dv2[0] = u;         dv2[1] = v - 1.0;
            dv3[0] = u - 1.0;   dv3[1] = v - 1.0;
            dv4[0] = u - 1.0;   dv4[1] = v;

            g1 = dot2(gv1, dv1);
            g2 = dot2(gv2, dv2);
            g3 = dot2(gv3, dv3);
            g4 = dot2(gv4, dv4);

            value = lerp(ease(v), lerp(ease(u), g1, g4), lerp(ease(u), g2, g3));

            min = value < min ? value : min;

            if (additive)
                array[j*size + i] += value;
            else
                array[j*size + i] = value;
        }
    }

    for (j = 0; j < size; j++)
    {
        for (i = 0; i < size; i++)
        {
            array[j*size + i] -= min;
            array[j*size + i] = pow(array[j*size + i], exponent);
        }
    }

    free(gradients);
}

static float gaussian(float x, float y, float sigma)
{
    return exp(-(x*x + y*y)/(sigma*sigma*2));
}

static float convol(const float* array, int size, const float* matrix, int matSize, int x, int y)
{
    float res = 0.0;
    int i, j;
    int radius = matSize/2;

    for (j = 0; j < matSize; j++)
    {
        for (i = 0; i < matSize; i++)
        {
            int xc = i/2 - radius + x < 0 ? radius - i/2 + x : i/2 - radius + x;
            int yc = j/2 - radius + y < 0 ? radius - j/2 + y : j/2 - radius + y;
            xc = xc > size ? size - 1 : xc;
            yc = yc > size ? size - 1 : yc;

            res += array[size*yc + xc] * matrix[j*matSize + i];
        }
    }

    return res;
}

static void smooth(float* out, float* in, int size, int radius, float factor)
{
    int matSize = 2 * radius + 1;
    float* convolMat = malloc(sizeof(int) * matSize * matSize);
    int i,j;
    float gaussianFactor = 0.0;

    for (j = 0; j < matSize; j++)
    {
        for (i = 0; i < matSize; i++)
        {
            convolMat[matSize*j + i] = 1.0;/*gaussian(i - radius, j - radius, sigma);*/
            gaussianFactor += convolMat[matSize*j + i];
        }
    }

    for (i = 0; i < matSize*matSize; i++)
        convolMat[i] /= gaussianFactor;

    for (j = 0; j < size; j++)
    {
        for (i = 0; i < size; i++)
        {
            out[size*j + i] = convol(in, size, convolMat, matSize, i, j);
        }
    }

    free(convolMat);
}

void gen_diamond_square(float* heightmap, const float* pondmap, int size)
{
    int h = size;
    int i = h - 1;
    int id;
    float k = 1.2;
    /*float factor = 0.15;*/
    int smoothRadius = 1;
    float* hardmap = malloc(sizeof(float) * size * size);


    hardmap[0] = rand_float(-size, size)/k * pondmap[0];
    hardmap[size - 1] = rand_float(-size, size)/k * pondmap[size - 1];
    hardmap[(size - 1) * size] = rand_float(-size, size)/k * pondmap[(size - 1)*size];
    hardmap[size * size - 1] = rand_float(-size, size)/k * pondmap[size * size - 1];

    while (i > 1)
    {
        int x,y;
        id = i/2;

        for (x = id; x < h; x += i)
            for (y = id; y < h; y += i)
            {
                hardmap[size * y + x] = (hardmap[size * (y - id) + x - id] + hardmap[size * (y + id) + x - id]
                        + hardmap[size * (y - id) + x + id] + hardmap[size * (y + id) + x + id]) / 4.0
                        + rand_float(-id,id)/k*pondmap[y*size + x];
                    /*+ rand_float(-id,id)/k * pondmap[y*size + x] / 3.0;*/
            }

        for (x = 0; x < h; x += id)
        {
            int shift = x % i == 0 ? id : 0;

            for (y = shift; y < h; y += i)
            {
                float somme = 0.0;
                float n = 0.0;

                if (x >= id)
                {
                    somme += hardmap[y * size + x - id];
                    n++;
                }
                if (x + id < h)
                {
                    somme += hardmap[y * size + x + id];
                    n++;
                }
                if (y >= id)
                {
                    somme += hardmap[(y-id) * size + x];
                    n++;
                }
                if (y + id < h)
                {
                    somme += hardmap[(y+id) * size + x];
                    n++;
                }
                hardmap[y*size + x] = somme/n + rand_float(-id,id)/k*pondmap[y*size + x];
            }
        }
        /*k += factor*k;*/
        i = id;
    }

    smooth(heightmap, hardmap, size, smoothRadius, 60.0);

    free(hardmap);
}

int bernoulli(float prob)
{
    return (rand_float(0.0,1.0) < prob);
}
