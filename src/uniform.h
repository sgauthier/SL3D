#ifndef UNIFORM_H
#define UNIFORM_H

#include "light.h"
#include "camera.h"

struct Uniforms
{
    struct Camera camera;
    struct DirectionnalLight directionnalLights[10];
    int nbDirectionnalLights;
    struct LocalLight localLights[10];
    int nbLocalLights;
};

#endif
