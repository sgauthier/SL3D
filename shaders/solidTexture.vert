#version 130

in vec3 in_Vertex;
in vec2 in_TexCoord;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 solidColor;

out vec2 coordTexture;
out vec4 color;

void main()
{
    mat4 centeredView = mat4(mat3(view));
    gl_Position = projection * centeredView * model * vec4(in_Vertex, 1.0);
    color = vec4(solidColor, 1.0);

    coordTexture = in_TexCoord;
}
