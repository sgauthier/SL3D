#ifndef FLAG_H
#define FLAG_H

#include "map.h"
#include "renderable.h"

Renderable* flag_new(Map* map, GLuint shader);

#endif
