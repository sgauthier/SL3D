#include <GL/glew.h>

#include "material.h"

#ifndef RESOURCES_H
#define RESOURCES_H

enum Shaders
{
    SHADER_SOLID_COLOR = 0,
    SHADER_SOLID_TEXTURE,
    SHADER_BASIC,
    SHADER_FOG,
    SHADER_MINIMAP,
    NB_SHADERS
};

enum Textures
{
    TEXTURE_TUX = 0,
    TEXTURE_TUX_RGBA,
    TEXTURE_SKYBOX,
    TEXTURE_SNOW,
    TEXTURE_DOG,
    TEXTURE_TREE,
    TEXTURE_FLAG,
    TEXTURE_SLED,
    NB_TEXTURES
};

extern GLuint shaders[NB_SHADERS];
extern GLuint textures[NB_TEXTURES];
extern const struct Material *const defaultMaterial;

int resources_init(void);
void resources_free(void);

#endif
