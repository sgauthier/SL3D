#ifndef FOREST_H
#define FOREST_H

#include "map.h"

void forest_generate(Map* map, float density, float compactness, int forestSize,
                     GLuint shader);

#endif
