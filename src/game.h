#ifndef GAME_H
#define GAME_H

typedef struct Game Game;

struct Params
{
    int fog;

    float forestDensity;
    float forestCompactness;

    float terrainRoughness;
};

Game* game_new(struct Params* params);
void game_free(Game* game);

int game_is_running(Game* game);
void game_update(Game* game);

#endif
