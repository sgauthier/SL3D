#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <GL/glew.h>

#include "renderable.h"
#include "camera.h"
#include "model.h"
#include "linear_algebra.h"
#include "resources.h"
#include "renderables/cube.h"

#define DEBUG 0
#define NEAR_TO_FAR 50
#define FAR_TO_INF 250

static Vec3 cameraPosition;
static Mat4 curModel;

struct GLObject
{
    unsigned int numVertices;
    GLuint texture;
    GLuint vao;
    GLuint vboVert;
    GLuint vboNorm;
    GLuint vboTexCoords;
};

struct Renderable
{
    struct Renderable** children;
    struct Renderable* parent;
    unsigned int nbChildren;

    struct Model model;
    Mat3 nit;
    struct Material material;
    int drawLines;
    int alwaysDraw;
    Vec3 solidColor;
    Vec3 boundingBox;
    Vec3 boundingBoxCenter;
    char name[100];

    struct GLObject glModels[NB_DIST_LEVELS];

    GLuint shader;
};

static void renderable_update_nit(struct Renderable* renderable, Mat4 model)
{
    Mat3 tmp;
    mat4to3(tmp, model);
    invert3m(renderable->nit, tmp);
    transpose3m(renderable->nit);
}

static void rec_update_nit_aux(struct Renderable* renderable, Mat4 model)
{
    Mat4 tmp;
    int i;

    renderable_update_nit(renderable, model);

    for (i = 0; i < renderable->nbChildren; i++)
    {
        mul4mm(tmp, model, renderable->children[i]->model.matrix);
        rec_update_nit_aux(renderable->children[i], tmp);
    }
}

void renderable_update_all_nits(struct Renderable* renderable)
{
    Renderable* root = renderable;
    Mat4 model, tmp;

    memcpy(model, renderable->model.matrix, sizeof(Mat4));

    while (root->parent)
    {
        root = root->parent;
        mul4mm(tmp, root->model.matrix, model);
        memcpy(model, tmp, sizeof(Mat4));
    }

    rec_update_nit_aux(renderable, model);
}

static void compute_bounding_box(Vec3 bb, Vec3 bbc, const float* vertices, unsigned int numVertices)
{
    float maxX = -FLT_MAX, maxY = -FLT_MAX, maxZ = -FLT_MAX, minX = FLT_MAX, minY = FLT_MAX, minZ = FLT_MAX;
    int i;

    for (i = 0; i < numVertices * 3; i += 3)
    {
        maxX = vertices[i] > maxX ? vertices[i] : maxX;
        minX = vertices[i] < minX ? vertices[i] : minX;

        maxY = vertices[i + 1] > maxY ? vertices[i + 1] : maxY;
        minY = vertices[i + 1] < minY ? vertices[i + 1] : minY;

        maxZ = vertices[i + 2] > maxZ ? vertices[i + 2] : maxZ;
        minZ = vertices[i + 2] < minZ ? vertices[i + 2] : minZ;
    }

    bb[0] = maxX - minX;
    bb[1] = maxY - minY;
    bb[2] = maxZ - minZ;

    bbc[0] = (maxX + minX) / 2.0;
    bbc[1] = (maxY + minY) / 2.0;
    bbc[2] = (maxZ + minZ) / 2.0;
}

static void load_gl_object(struct GLObject* GLO, unsigned int numVertices, float* vertices, float* normals, 
                    float* texCoords, GLuint texture)
{
    GLO->numVertices = numVertices;
    GLO->texture = texture;
    if (numVertices)
    {
        glGenBuffers(1, &GLO->vboVert);
        glBindBuffer(GL_ARRAY_BUFFER, GLO->vboVert);
        glBufferData(GL_ARRAY_BUFFER, numVertices * 3 * sizeof(float), vertices, GL_STATIC_DRAW);

        glGenBuffers(1, &GLO->vboNorm);
        glBindBuffer(GL_ARRAY_BUFFER, GLO->vboNorm);
        glBufferData(GL_ARRAY_BUFFER, numVertices * 3 * sizeof(float), normals, GL_STATIC_DRAW);

        if (texCoords)
        {
            glGenBuffers(1, &GLO->vboTexCoords);
            glBindBuffer(GL_ARRAY_BUFFER, GLO->vboTexCoords);
            glBufferData(GL_ARRAY_BUFFER, numVertices * 2 * sizeof(float), texCoords, GL_STATIC_DRAW);
        }

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glGenVertexArrays(1, &GLO->vao);
        glBindVertexArray(GLO->vao);

        glBindBuffer(GL_ARRAY_BUFFER, GLO->vboVert);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, GLO->vboNorm);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

        if (GLO->texture && texCoords)
        {
            glBindBuffer(GL_ARRAY_BUFFER, GLO->vboTexCoords);
            glEnableVertexAttribArray(2);
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }

        glBindVertexArray(0);
    }
    else
    {
        GLO->vao = 0;
        GLO->vboVert = 0;
        GLO->vboNorm = 0;
        GLO->vboTexCoords = 0;
    }
}

struct Renderable* renderable_new(unsigned int numVertices, float* vertices, float* normals,
                    float* texCoords, GLuint shader, GLuint texture)
{
    struct Renderable* renderable;

    if (!(renderable = malloc(sizeof(struct Renderable))))
    {
        fprintf(stderr, "Error: failed to allocate a Renderable\n");
        return NULL;
    }

    model_init(&renderable->model);
    load_id3(renderable->nit);

    renderable->children = NULL;
    renderable->parent = NULL;
    renderable->nbChildren = 0;
    renderable->shader = shader;
    renderable->drawLines = 0;
    renderable->alwaysDraw = 0;
    renderable->name[0] = '\0';
    compute_bounding_box(renderable->boundingBox, renderable->boundingBoxCenter, vertices, numVertices);

    /*By default:
     *  - Objects at NEAR are drawn completely,
     *  - Objects at FAR are drawn with the same mesh but without texture
     *  - Objects at INF are not drawn
     *It is possible to change this with renderable_set_reduced_model
     */

    load_gl_object(&renderable->glModels[NEAR], numVertices, vertices, normals,
                    texCoords, texture);

    load_gl_object(&renderable->glModels[FAR], numVertices, vertices, normals,
                    NULL, 0);

    load_gl_object(&renderable->glModels[INF], 0, NULL, NULL,
                    NULL, 0);

    if (DEBUG)
    {
        printf("Loading new Renderable: %d vertices\nBB: %f-%f-%f, W: %f, H: %f, D: %f\n", numVertices,
                renderable->boundingBoxCenter[0], renderable->boundingBoxCenter[1], renderable->boundingBoxCenter[2],
                renderable->boundingBox[0], renderable->boundingBox[1], renderable->boundingBox[2]);
    }

    return renderable;
}

Renderable* renderable_copy(Renderable* source)
{
    Renderable* ret = malloc(sizeof(Renderable));
    memcpy(ret, source, sizeof(Renderable));

    return ret;
}

void renderable_set_reduced_model(Renderable* renderable, enum DistancesLevel level, unsigned int numVertices,
                                  float* vertices,float* normals, float* texCoords)
{
    load_gl_object(&renderable->glModels[level], numVertices, vertices, normals, texCoords,
                   renderable->glModels[NEAR].texture);
}

void renderable_free(struct Renderable* renderable)
{
    if (renderable)
    {
        int i;
        for (i = 0; i < NB_DIST_LEVELS; i++)
        {
            if (glIsBuffer(renderable->glModels[i].vboVert) == GL_TRUE)
                glDeleteBuffers(1, &renderable->glModels[i].vboVert);
            if (glIsBuffer(renderable->glModels[i].vboNorm) == GL_TRUE)
                glDeleteBuffers(1, &renderable->glModels[i].vboNorm);
            if (glIsBuffer(renderable->glModels[i].vboTexCoords) == GL_TRUE)
                glDeleteBuffers(1, &renderable->glModels[i].vboTexCoords);
            if (glIsVertexArray(renderable->glModels[i].vao) == GL_TRUE)
                glDeleteVertexArrays(1, &renderable->glModels[i].vao);
        }

        for (i = 0; i < renderable->nbChildren; i++)
        {
            renderable_free(renderable->children[i]);
        }

        free(renderable->children);
        free(renderable);
    }
}

void renderable_set_material(struct Renderable* renderable, const struct Material* material)
{
    memcpy(&renderable->material, material, sizeof(struct Material));
}

void renderable_set_solid_color(struct Renderable* renderable, float r, float g, float b)
{
    renderable->solidColor[0] = r;
    renderable->solidColor[1] = g;
    renderable->solidColor[2] = b;
}

static void load_lights(GLuint shader, struct Uniforms* uniforms)
{
    int i;
    char locName[100];

    glUniform1i(glGetUniformLocation(shader, "nbDirectionnalLights"), uniforms->nbDirectionnalLights);
    glUniform1i(glGetUniformLocation(shader, "nbLocalLights"), uniforms->nbLocalLights);

    for (i = 0; i < uniforms->nbDirectionnalLights; i++)
    {
        sprintf(locName, "directionnalLights[%d].direction", i);
        glUniform3fv(glGetUniformLocation(shader, locName), 1, (float*)uniforms->directionnalLights[i].direction);
        sprintf(locName, "directionnalLights[%d].ambient", i);
        glUniform3fv(glGetUniformLocation(shader, locName), 1, (float*)uniforms->directionnalLights[i].ambient);
        sprintf(locName, "directionnalLights[%d].diffuse", i);
        glUniform3fv(glGetUniformLocation(shader, locName), 1, (float*)uniforms->directionnalLights[i].diffuse);
        sprintf(locName, "directionnalLights[%d].specular", i);
        glUniform3fv(glGetUniformLocation(shader, locName), 1, (float*)uniforms->directionnalLights[i].specular);
    }

    for (i = 0; i < uniforms->nbLocalLights; i++)
    {
        sprintf(locName, "localLights[%d].position", i);
        glUniform3fv(glGetUniformLocation(shader, locName), 1, (float*)uniforms->localLights[i].position);
        sprintf(locName, "localLights[%d].intensity", i);
        glUniform1fv(glGetUniformLocation(shader, locName), 1, &uniforms->localLights[i].intensity);
        sprintf(locName, "localLights[%d].ambient", i);
        glUniform3fv(glGetUniformLocation(shader, locName), 1, (float*)uniforms->localLights[i].ambient);
        sprintf(locName, "localLights[%d].diffuse", i);
        glUniform3fv(glGetUniformLocation(shader, locName), 1, (float*)uniforms->localLights[i].diffuse);
        sprintf(locName, "localLights[%d].specular", i);
        glUniform3fv(glGetUniformLocation(shader, locName), 1, (float*)uniforms->localLights[i].specular);
    }
}

static void load_material(GLuint shader, struct Material* material)
{
    glUniform3fv(glGetUniformLocation(shader, "material.ambient"), 1, (float*)material->ambient);
    glUniform3fv(glGetUniformLocation(shader, "material.diffuse"), 1, (float*)material->diffuse);
    glUniform3fv(glGetUniformLocation(shader, "material.specular"), 1, (float*)material->specular);
    glUniform1fv(glGetUniformLocation(shader, "material.shininess"), 1, &material->shininess);
}

static int box_in_frustrum(Vec3 box, Vec3 center, Mat4 projCam, Mat4 model)
{
    Mat4 projMat;
    Vec4 projPos;
    Vec4 currentPoint;
    int i = 0;
    int outAreas[5] = {0,0,0,0,0};

    mul4mm(projMat, projCam, model);

    while (i < 8)
    {
        currentPoint[0] = center[0] + ((float)(2*(i%2)-1)) * box[0]/2.0;
        currentPoint[1] = center[1] + ((float)(2*((i/2)%2)-1)) * box[1]/2.0;
        currentPoint[2] = center[2] + ((float)(2*((i/4)%2)-1)) * box[2]/2.0;
        currentPoint[3] = 1.0;
        mul4mv(projPos, projMat, currentPoint);

        if (projPos[0]/projPos[3] < -1.0)
            outAreas[0]++;
        if (projPos[0]/projPos[3] > 1.0)
            outAreas[1]++;
        if (projPos[1]/projPos[3] < -1.0)
            outAreas[2]++;
        if (projPos[1]/projPos[3] > 1.0)
            outAreas[3]++;
        if (projPos[2] < 0)
            outAreas[4]++;

        i++;
    }

    return !(outAreas[0] == 8 || outAreas[1] == 8 || outAreas[2] == 8 || outAreas[3] == 8 || outAreas[4] == 8);
}

static enum DistancesLevel box_distance(Vec3 box, Vec3 center, struct Camera* cam, Mat4 model)
{
    int i = 0;
    Vec4 currentPoint;
    Vec4 currentWorldPoint;
    float ntf = NEAR_TO_FAR * NEAR_TO_FAR;
    float fti = FAR_TO_INF * FAR_TO_INF;
    enum DistancesLevel min = INF;

    for (i = 0; i < 8; i++)
    {
        float x, y, z;
        currentPoint[0] = center[0] + ((float)(2*(i%2)-1)) * box[0]/2.0;
        currentPoint[1] = center[1] + ((float)(2*((i/2)%2)-1)) * box[1]/2.0;
        currentPoint[2] = center[2] + ((float)(2*((i/4)%2)-1)) * box[2]/2.0;
        currentPoint[3] = 1.0;

        mul4mv(currentWorldPoint, model, currentPoint);

        x = currentWorldPoint[0] - cam->position[0];
        y = currentWorldPoint[1] - cam->position[1];
        z = currentWorldPoint[2] - cam->position[2];

        if (x*x + y*y + z*z < ntf)
            return NEAR;

        if (x*x + y*y + z*z < fti)
            min = FAR;
    }

    return min;
}

float distance_to_camera(const Renderable* r)
{
    Vec4 tmp, pc;
    Mat4 model;
    float d;

    tmp[0] = r->boundingBoxCenter[0];
    tmp[1] = r->boundingBoxCenter[1];
    tmp[2] = r->boundingBoxCenter[2];
    tmp[3] = 1;
    mul4mm(model, curModel, r->model.matrix);
    mul4mv(pc, model, tmp);

    d = (pc[0] - cameraPosition[0]) * (pc[0] - cameraPosition[0]) +
           (pc[1] - cameraPosition[1]) * (pc[1] - cameraPosition[1]) +
           (pc[2] - cameraPosition[2]) * (pc[2] - cameraPosition[2]);

    return d;
}

int comp_children(const void* c1, const void* c2)
{
    const Renderable* child1 = *(Renderable* const*)c1;
    const Renderable* child2 = *(Renderable* const*)c2;
    float d1, d2;

    d1 = distance_to_camera(child1);
    d2 = distance_to_camera(child2);
    return (d1 < d2) - (d2 < d1);
}

int renderable_draw_aux(struct Renderable* renderable, struct Uniforms* uniforms, Mat4 model)
{
    Mat4 proj;
    mul4mm(proj, uniforms->camera.projection, uniforms->camera.view);
    if (renderable->alwaysDraw || box_in_frustrum(renderable->boundingBox, renderable->boundingBoxCenter, proj, model))
    {
        Mat4 childModel;
        unsigned int i;
        unsigned int cpt = 0;
        enum DistancesLevel dist = renderable->alwaysDraw ? NEAR : box_distance(renderable->boundingBox,
                renderable->boundingBoxCenter, &uniforms->camera, model);

        if (renderable->glModels[dist].numVertices)
        {
            glUseProgram(renderable->shader);
            {
                glBindVertexArray(renderable->glModels[dist].vao);
                {
                    glUniformMatrix4fv(glGetUniformLocation(renderable->shader, "projection"),
                            1, GL_FALSE, (float*)uniforms->camera.projection);
                    glUniformMatrix4fv(glGetUniformLocation(renderable->shader, "view"),
                            1, GL_FALSE, (float*)uniforms->camera.view);
                    glUniformMatrix4fv(glGetUniformLocation(renderable->shader, "model"),
                            1, GL_FALSE, (float*)model);
                    glUniformMatrix3fv(glGetUniformLocation(renderable->shader, "NIT"),
                            1, GL_FALSE, (float*)renderable->nit);
                    glUniform3fv(glGetUniformLocation(renderable->shader, "cameraPosition"),
                            1, (float*)uniforms->camera.position);
                    glUniform3fv(glGetUniformLocation(renderable->shader, "solidColor"),
                            1, renderable->solidColor);
                    load_lights(renderable->shader, uniforms);
                    load_material(renderable->shader, &renderable->material);
                    if (renderable->glModels[dist].texture)
                        glBindTexture(GL_TEXTURE_2D, renderable->glModels[dist].texture);

                    if (renderable->drawLines)
                        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                    else
                        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

                    glDrawArrays(GL_TRIANGLES, 0, renderable->glModels[dist].numVertices);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                glBindVertexArray(0);
            }
            glUseProgram(0);
        }

        memcpy(cameraPosition, uniforms->camera.position, sizeof(Vec3));
        memcpy(curModel, model, sizeof(Mat4)); /*Hack dégueulasse pour passer la matrice modèle à qsort: on la met en global*/
        qsort(renderable->children, renderable->nbChildren, sizeof(Renderable*), comp_children);
        for (i = 0; i < renderable->nbChildren; i++)
        {
            mul4mm(childModel, model, renderable->children[i]->model.matrix);
            cpt += renderable_draw_aux(renderable->children[i], uniforms, childModel);
        }

        return 1 + cpt;
    }
    else
    {
        return 0;
    }
}

int renderable_draw(struct Renderable* renderable, struct Uniforms* uniforms)
{
    return renderable_draw_aux(renderable, uniforms, renderable->model.matrix);
}

void renderable_display_bounding_box(Renderable* renderable)
{
    Renderable* bb;
    int i;

    for (i = 0; i < renderable->nbChildren; i++)
    {
        renderable_display_bounding_box(renderable->children[i]);
    }

    bb = cube_new(2, shaders[SHADER_SOLID_COLOR], 0);
    bb->drawLines = 1;
    renderable_move(bb, renderable->boundingBoxCenter);
    bb->model.matrix[0][0] = renderable->boundingBox[0]/2.0;
    bb->model.matrix[1][1] = renderable->boundingBox[1]/2.0;
    bb->model.matrix[2][2] = renderable->boundingBox[2]/2.0;

    bb->solidColor[0] = 0.0;
    bb->solidColor[1] = 1.0;
    bb->solidColor[2] = 0.0;
    renderable_add_child(renderable, bb);
}

void renderable_toggle_always_draw(Renderable* renderable)
{
    renderable->alwaysDraw = !renderable->alwaysDraw;
}

static void update_parent_bounding_box(Vec3 pbb, Vec3 pbbc, Vec3 cbb, Vec3 cbbc)
{
    float minXp = pbbc[0] - pbb[0]/2.0; float maxXp = pbbc[0] + pbb[0]/2.0;
    float minYp = pbbc[1] - pbb[1]/2.0; float maxYp = pbbc[1] + pbb[1]/2.0;
    float minZp = pbbc[2] - pbb[2]/2.0; float maxZp = pbbc[2] + pbb[2]/2.0;

    float minXc = cbbc[0] - cbb[0]/2.0; float maxXc = cbbc[0] + cbb[0]/2.0;
    float minYc = cbbc[1] - cbb[1]/2.0; float maxYc = cbbc[1] + cbb[1]/2.0;
    float minZc = cbbc[2] - cbb[2]/2.0; float maxZc = cbbc[2] + cbb[2]/2.0;

    float newMinXp = minXc < minXp ? minXc : minXp;
    float newMinYp = minYc < minYp ? minYc : minYp;
    float newMinZp = minZc < minZp ? minZc : minZp;
    float newMaxXp = maxXc > maxXp ? maxXc : maxXp;
    float newMaxYp = maxYc > maxYp ? maxYc : maxYp;
    float newMaxZp = maxZc > maxZp ? maxZc : maxZp;

    pbb[0] = newMaxXp - newMinXp;
    pbb[1] = newMaxYp - newMinYp;
    pbb[2] = newMaxZp - newMinZp;
    pbbc[0] = (newMaxXp + newMinXp)/2.0;
    pbbc[1] = (newMaxYp + newMinYp)/2.0;
    pbbc[2] = (newMaxZp + newMinZp)/2.0;
}

static void update_all_parents_bb(Renderable* child)
{
    Renderable* cur = child;

    while (cur)
    {
        if (cur->parent)
            update_parent_bounding_box(cur->parent->boundingBox, cur->parent->boundingBoxCenter,
                    cur->boundingBox, cur->boundingBoxCenter);
        cur = cur->parent;
    }
}

int renderable_add_child(Renderable* parent, Renderable* child)
{
    Renderable** tmp;

    if ((tmp = realloc(parent->children, (parent->nbChildren + 1) * sizeof(struct Renderable*))))
    {
        parent->children = tmp;
        tmp[parent->nbChildren++] = child;
        child->parent = parent;

        /*update_all_parents_bb(child);*/
        return 1;
    }

    return 0;
}

Renderable* renderable_get_child(Renderable* parent, int index)
{
    return parent->children[index];
}

void renderable_move(struct Renderable* renderable, Vec3 translation)
{
    model_move(&renderable->model, translation);
}

void renderable_rotate(struct Renderable* renderable, Vec3 axis, float angle)
{
    model_rotate(&renderable->model, axis, angle);
    renderable_update_all_nits(renderable);
}

void renderable_scale(Renderable* renderable, float scale)
{
    model_scale(&renderable->model, scale);
}

void renderable_set_name(Renderable* renderable, char* name)
{
    if (strlen(name) < 100)
        strcpy(renderable->name, name);
}

void static aux_print_renderable(Renderable* renderable, int indent)
{
    int i;
    for (i = 0; i < indent; i++)
        printf("  > ");

    printf("Renderable: %d vertices, BB: %f/%f/%f W: %f, H: %f, D: %f\n", renderable->glModels[0].numVertices,
            renderable->boundingBoxCenter[0], renderable->boundingBoxCenter[1], renderable->boundingBoxCenter[2],
            renderable->boundingBox[0], renderable->boundingBox[1], renderable->boundingBox[2]);

    for (i = 0; i < renderable->nbChildren; i++)
        aux_print_renderable(renderable->children[i], indent + 1);
}

void print_renderable(Renderable* renderable)
{
    aux_print_renderable(renderable, 0);
}

struct Model* renderable_get_model(Renderable* renderable)
{
    return &renderable->model;
}
