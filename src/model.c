#include <string.h>
#include "model.h"
#include "linear_algebra.h"
#include "quaternion.h"

void model_init(struct Model* model)
{
    model->position[0] = 0;
    model->position[1] = 0;
    model->position[2] = 0;
    model->orientation[0] = 1;
    model->orientation[1] = 0;
    model->orientation[2] = 0;
    model->orientation[3] = 0;
    load_id4(model->matrix);
}

static void model_update_position(struct Model* model)
{
    memcpy(model->matrix[3], model->position, sizeof(Vec3));
}

static void model_update_orientation(struct Model* model)
{
    Mat3 rot;
    quaternion_to_mat3(rot, model->orientation);
    mat3to4(model->matrix, rot);
}

void model_move(struct Model* model, Vec3 translation)
{
    incr3v(model->position, translation);
    model_update_position(model);
}

void model_rotate_q(struct Model* model, Quaternion q)
{
    Quaternion old;
    memcpy(old, model->orientation, sizeof(Quaternion));
    quaternion_mul(model->orientation, q, old);
    model_update_orientation(model);
}

void model_rotate(struct Model* model, Vec3 axis, float angle)
{
    Quaternion q;
    quaternion_set_axis_angle(q, axis, angle);
    model_rotate_q(model, q);
}

void model_update(struct Model* model)
{
    model_update_position(model);
    model_update_orientation(model);
}

void model_update_from_matrix(struct Model* model)
{
    Mat3 rot;
    memcpy(model->position, model->matrix[3], sizeof(Vec3));
    mat4to3(rot, model->matrix);
    quaternion_from_mat3(model->orientation, rot);
}

void model_scale(struct Model* model, float scale)
{
    Mat4 tmp;
    memcpy(tmp, model->matrix, sizeof(Mat4));
    mul4sm(model->matrix, scale, tmp);
}

#define IS_A_NUMBER(x) ((x) == (x))
int model_is_valid(struct Model* model)
{
    return IS_A_NUMBER(model->position[0])
        && IS_A_NUMBER(model->position[1])
        && IS_A_NUMBER(model->position[2])
        && IS_A_NUMBER(model->orientation[0])
        && IS_A_NUMBER(model->orientation[1])
        && IS_A_NUMBER(model->orientation[2])
        && IS_A_NUMBER(model->orientation[3]);
}
