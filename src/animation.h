#ifndef ANIMATION_H
#define ANIMATION_H

#include "renderable.h"

struct KeyFrame
{
    float t;
    float angle;
};

typedef struct Animable
{
    Renderable* renderable;
    struct KeyFrame curFrame;
    struct KeyFrame** keyFrames;
    int* nbFrames;
    int nbAnimations;
    float speedFactor
} Animable;

Animable* animable_new(Renderable* renderable);

/*Keyframes must be added chronologically*/
int animable_add_animation(Animable* renderable);
void animable_add_keyframe(Animable* animable, int animation,
                           float t, float angle);

/*A keyframe cycle is in a time span of [0.0;1.0]*/
void animable_animate(Animable* animable, int animation, float dt);

void animable_free(Animable* animable);

#endif
