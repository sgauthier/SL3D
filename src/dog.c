#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include "dog.h"
#include "sled.h"
#include "renderables/mesh.h"
#include "resources.h"
#include "animation.h"
#include "linear_algebra.h"
#include "quaternion.h"
#include "map.h"

#define ROPE_SIZE 4.0

struct Dog
{
    enum DogStatus status;
    Animable* trunk;
    Animable* paws[4];
    float forceNorm;
    Vec3 traction;
    Vec3 goal;

    enum PullType pullType;
    void* pulledObject;
};

Dog* dog_new(enum PullType pullType, void* pulledObject)
{
    Dog* res = malloc(sizeof(Dog));

    if (res)
    {
        Vec3 front = {0.205, 0.413, 0};
        Vec3 back = {-0.257, 0.437, 0};
        Renderable* tmp;
        int i;

        tmp = mesh_new("models", "dog-trunk.obj", shaders[SHADER_BASIC], textures[TEXTURE_DOG]);
        renderable_set_solid_color(tmp, 0.4, 0.4, 0.4);
        res->trunk = animable_new(tmp);
        res->trunk->speedFactor = 2.3;
        for (i = 0; i < DOG_NB_STATUS; i++)
            animable_add_animation(res->trunk);
        animable_add_keyframe(res->trunk, DOG_IDLE, 0, 0);
        animable_add_keyframe(res->trunk, DOG_WALKING, 0, -0.02);
        animable_add_keyframe(res->trunk, DOG_WALKING, 0.5, 0.02);
        animable_add_keyframe(res->trunk, DOG_RUNNING, 0, -0.1);
        animable_add_keyframe(res->trunk, DOG_RUNNING, 0.5, 0.1);

        tmp = mesh_new("models", "dog-f-l-paw.obj", shaders[SHADER_BASIC], textures[TEXTURE_DOG]);
        renderable_move(tmp, front);
        renderable_set_name(tmp, "pawFL");
        res->paws[0] = animable_new(tmp);
        res->paws[0]->speedFactor = 2.3;
        tmp = mesh_new("models", "dog-f-r-paw.obj", shaders[SHADER_BASIC], textures[TEXTURE_DOG]);
        renderable_set_name(tmp, "pawFR");
        renderable_move(tmp, front);
        res->paws[1] = animable_new(tmp);
        res->paws[1]->speedFactor = 2.3;
        tmp = mesh_new("models", "dog-b-l-paw.obj", shaders[SHADER_BASIC], textures[TEXTURE_DOG]);
        renderable_set_name(tmp, "pawBL");
        renderable_move(tmp, back);
        res->paws[2] = animable_new(tmp);
        res->paws[2]->speedFactor = 2.3;
        tmp = mesh_new("models", "dog-b-r-paw.obj", shaders[SHADER_BASIC], textures[TEXTURE_DOG]);
        renderable_set_name(tmp, "pawBR");
        renderable_move(tmp, back);
        res->paws[3] = animable_new(tmp);
        res->paws[3]->speedFactor = 2.3;

        for (i = 0; i < 4; i++)
        {
            int j;
            renderable_set_solid_color(res->paws[i]->renderable, 0.4, 0.4, 0.4);
            renderable_add_child(res->trunk->renderable, res->paws[i]->renderable);
            for (j = 0; j < DOG_NB_STATUS; j++)
                animable_add_animation(res->paws[i]);

            animable_add_keyframe(res->paws[i], DOG_IDLE, 0, 0);
        }

        animable_add_keyframe(res->paws[0], DOG_WALKING, 0, -0.3);
        animable_add_keyframe(res->paws[0], DOG_WALKING, 0.5, 0.3);

        animable_add_keyframe(res->paws[1], DOG_WALKING, 0.5, -0.3);
        animable_add_keyframe(res->paws[1], DOG_WALKING, 1, 0.3);

        animable_add_keyframe(res->paws[2], DOG_WALKING, 0.25, -0.3);
        animable_add_keyframe(res->paws[2], DOG_WALKING, 0.75, 0.3);

        animable_add_keyframe(res->paws[3], DOG_WALKING, 0.25, 0.3);
        animable_add_keyframe(res->paws[3], DOG_WALKING, 0.75, -0.3);

        animable_add_keyframe(res->paws[0], DOG_RUNNING, 0, -0.7);
        animable_add_keyframe(res->paws[0], DOG_RUNNING, 0.5, 0.8);

        animable_add_keyframe(res->paws[1], DOG_RUNNING, 0.2, -0.7);
        animable_add_keyframe(res->paws[1], DOG_RUNNING, 0.7, 0.8);

        animable_add_keyframe(res->paws[2], DOG_RUNNING, 0, 0.83);
        animable_add_keyframe(res->paws[2], DOG_RUNNING, 0.5, -0.7);

        animable_add_keyframe(res->paws[3], DOG_RUNNING, 0.18, 0.8);
        animable_add_keyframe(res->paws[3], DOG_RUNNING, 0.58, -0.75);

        res->status = DOG_IDLE;
        res->forceNorm = 0;
        res->traction[0] = 0;
        res->traction[1] = 0;
        res->traction[2] = 0;
        res->pullType = pullType;
        res->pulledObject = pulledObject;

        return res;
    }
    else
    {
        fprintf(stderr, "Error: can't allocate memory for dog\n");
        return NULL;
    }
}

static void update_status(Dog* dog)
{
    if (dog->forceNorm == 0)
        dog->status = DOG_IDLE;
    else if (dog->forceNorm < 2)
        dog->status = DOG_WALKING;
    else
        dog->status = DOG_RUNNING;
}

void dog_incr_force(Dog* dog)
{
    if ((dog->forceNorm += 0.1) > 5.0)
        dog->forceNorm = 5.0;
    update_status(dog);
}

void dog_decr_force(Dog* dog)
{
    if ((dog->forceNorm -= 0.1) < 0.0)
        dog->forceNorm = 0.0;
    update_status(dog);
}

void dog_rotate(Dog* dog, float angle)
{
    Vec3 axis = {0, 1, 0};
    model_rotate(renderable_get_model(dog->trunk->renderable), axis, angle);
}

enum DogUpdateError dog_update(Dog* dog, struct Map* map, float dt)
{
    int i;
    float ps, angle, dist, dMin;
    Vec4 t, o, os, s, y, z, n, u = {0, 0, 0, 1}, v = {0, 1, 0, 0}, w = {0, 0, 1, 0}, x = {1, 0, 0, 0};
    struct Model* model = renderable_get_model(dog->trunk->renderable);
    struct Model backup = *model;
    Quaternion rot, swing, twist;
    enum DogUpdateError error;

    animable_animate(dog->trunk, dog->status, dt);

    for (i = 0; i < 4; i++)
        animable_animate(dog->paws[i], dog->status, dt);

    mul4mv(o, model->matrix, u);
    mul4mv(z, model->matrix, w);
    if (!map_get_normal(map, o[0], o[2], n))
        return DOG_OUT_OF_MAP;
    cross3(s, n, z);
    scale3v(s, dt * ((n[1] < 0) ? dog->forceNorm : (n[1] * dog->forceNorm)));
    incr3v(s, dog->traction);
    model_move(model, s);

    mul4mv(t, model->matrix, x);
    compute_rotation(t, dog->goal, o, &angle);
    quaternion_set_axis_angle(rot, o, angle);
    quaternion_decompose_swing_twist(rot, v, swing, twist);
    model_rotate_q(model, twist);

    mul4mv(o, model->matrix, u);
    s[0] = s[2] = 0;
    if ((s[1] = map_get_height(map, o[0], o[2]) - o[1]) == FLT_MAX)
        return DOG_OUT_OF_MAP;
    model_move(model, s);

    switch (dog->pullType)
    {
        case PULL_DOG:
            mul4mv(os, dog_get_model(dog->pulledObject)->matrix, u);
            mul4mv(o, model->matrix, u);
            decr3v(o, os);
            if (norm3(o) > ROPE_SIZE)
                memcpy(((Dog*)(dog->pulledObject))->traction, o, sizeof(Vec3));
            else
                memset(((Dog*)(dog->pulledObject))->traction, 0, sizeof(Vec3));
            memcpy(((Dog*)(dog->pulledObject))->goal, o, sizeof(Vec3));
            if ((error = dog_update(dog->pulledObject, map, dt)) != DOG_NO_ERROR)
                return error;
            mul4mv(os, dog_get_model(dog->pulledObject)->matrix, u);
            dMin = 0.77;
            break;
        case PULL_SLED:
            mul4mv(os, sled_get_model(dog->pulledObject)->matrix, u);
            mul4mv(o, model->matrix, u);
            decr3v(o, os);
            if (norm3(o) > ROPE_SIZE)
                mul3sv(((struct Sled*)(dog->pulledObject))->traction, 1.0 / dt, o);
            else
                memset(((struct Sled*)(dog->pulledObject))->traction, 0, sizeof(Vec3));
            if ((error = sled_update(dog->pulledObject, map, dt)) != SLED_NO_ERROR)
                return error;
            mul4mv(os, sled_get_model(dog->pulledObject)->matrix, u);
            dMin = 1.5;
            break;
        case PULL_NONE:
            dMin = 0.0;
            break;
    }
    mul4mv(o, model->matrix, u);
    decr3v(o, os);
    if ((dist = norm3(o)) > ROPE_SIZE)
    {
        scale3v(o, (ROPE_SIZE - dist) / dist);
        model_move(model, o);
    }
    else if (dist < dMin)
    {
        if (dog->pullType == PULL_SLED)
        {
            return DOG_COLLISION_WITH_SLED;
        }
        else
        {
            scale3v(o, (dMin - dist) / dist);
            model_move(model, o);
        }
    }

    mul4mv(o, model->matrix, u);
    if (!map_get_normal(map, o[0], o[2], n))
        return DOG_OUT_OF_MAP;
    mul4mv(y, model->matrix, v);
    cross3(s, y, n);
    ps = dot3(y, n),
    angle = (ps > 1.0) ? 0.0 : ((ps < -1.0) ? M_PI : acos(ps));
    quaternion_set_axis_angle(rot, s, angle);
    mul4mv(z, model->matrix, w);
    quaternion_decompose_swing_twist(rot, z, swing, twist);
    model_rotate_q(model, twist);
    s[0] = s[2] = 0;
    if ((s[1] = map_get_height(map, o[0], o[2]) - o[1]) == FLT_MAX)
        return DOG_OUT_OF_MAP;
    model_move(model, s);

    if (!model_is_valid(model))
        *model = backup;

    renderable_update_all_nits(dog_get_renderable(dog));

    return DOG_NO_ERROR;
}

Renderable* dog_get_renderable(Dog* dog)
{
    return dog->trunk->renderable;
}

struct Model* dog_get_model(Dog* dog)
{
    return renderable_get_model(dog->trunk->renderable);
}

void dog_free(Dog* dog)
{
    int i;

    animable_free(dog->trunk);

    for (i = 0; i < 4; i++)
        animable_free(dog->paws[i]);

    renderable_free(dog->trunk->renderable);
    free(dog);
}
