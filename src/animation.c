#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "animation.h"

Animable* animable_new(Renderable* renderable)
{
    Animable* res = malloc(sizeof(Animable));

    if (res)
    {
        res->renderable = renderable;
        res->nbAnimations = 0;
        res->nbFrames = NULL;
        res->keyFrames = NULL;
        
        res->curFrame.t = 0;
        res->curFrame.angle = 0;

        return res;
    }
    else
    {
        fprintf(stderr, "Error, could not create animation");
        return NULL;
    }
}

int animable_add_animation(Animable* animable)
{
    animable->nbAnimations++;
    animable->nbFrames = realloc(animable->nbFrames, animable->nbAnimations * sizeof(int));
    animable->keyFrames = realloc(animable->keyFrames, animable->nbAnimations * sizeof(struct KeyFrame*));
    animable->keyFrames[animable->nbAnimations-1] = NULL;
    animable->nbFrames[animable->nbAnimations-1] = 0;

    return animable->nbAnimations-1;
}

void animable_add_keyframe(Animable* animable, int animation, float t, float angle)
{
    animable->nbFrames[animation]++;
    animable->keyFrames[animation] = realloc(animable->keyFrames[animation], animable->nbFrames[animation] * sizeof(struct KeyFrame));
    animable->keyFrames[animation][animable->nbFrames[animation]-1].t = t;
    animable->keyFrames[animation][animable->nbFrames[animation]-1].angle = angle;
}

static void animate(struct KeyFrame* curFrame, struct KeyFrame* kfs, int nbKfs, float t)
{
    if (nbKfs >= 2)
    {
        struct KeyFrame start = kfs[nbKfs - 1];
        struct KeyFrame end = kfs[0];
        int i;
        float u;
        t = t - floor(t);

        for (i = 0; i < nbKfs; i++)
        {
            if (kfs[i].t > t)
            {
                end = kfs[i];
                start = i > 0 ? kfs[i-1] : kfs[nbKfs - 1];
                break;
            }
        }

        end.t = end.t < start.t ? 1 + end.t : end.t;
        t = t < start.t ? t + 1 : t;

        u = (t - start.t)/(end.t - start.t);
        u = (cos((1-u)*M_PI) + 1)/2;
        curFrame->t = t;
        curFrame->angle = start.angle * (1-u) + end.angle * u;
    }
    else if (nbKfs == 1)
    {
        *curFrame = kfs[0];
    }
}

void animable_animate(Animable* animable, int animation, float dt)
{
    struct KeyFrame tmp;
    float deltaAngle;
    Vec3 axis = {0,0,1};

    animate(&tmp, animable->keyFrames[animation], animable->nbFrames[animation], animable->curFrame.t + dt*animable->speedFactor);
    deltaAngle = tmp.angle - animable->curFrame.angle;
    renderable_rotate(animable->renderable, axis, deltaAngle);

    animable->curFrame = tmp;
}

void animable_free(Animable* animable)
{
    int i;

    for (i = 0; i < animable->nbAnimations; i++)
    {
        free(animable->keyFrames[i]);
    }
    free(animable->keyFrames);
    free(animable->nbFrames);
}
