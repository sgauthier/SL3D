#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "forest.h"
#include "tree.h"
#include "renderable.h"
#include "rand_tools.h"
#include "renderables/mesh.h"
#include "resources.h"

static void new_tree(Map* map, Renderable* mapTile, Renderable* treeTemplate, float x, float y, float scale)
{
    Vec3 translation;
    Renderable* newTree = renderable_copy(treeTemplate);

    translation[0] = x;
    translation[2] = y;
    translation[1] = map_get_height(map, translation[0], translation[2]);

    renderable_move(newTree, translation);
    renderable_scale(newTree, scale);

    renderable_add_child(mapTile, newTree);
}

void forest_generate(Map* map, float density, float compactness, int forestSize, GLuint shader)
{
    int cellSize = map->size / map->nbCellSize;
    int i,j,k;
    float* densityMap = malloc(sizeof(float) * map->size * map->size);
    Renderable* treeTemplate = tree_new(shader);

    printf("Generating forests...\n");

    gen_perlin(densityMap, map->size, forestSize, density, compactness, 0);

    for (i = 0; i < map->nbCellSize * map->nbCellSize; i++)
    {
        char rName[100];

        for (j = 0; j < cellSize; j++)
        {
            for (k = 0; k < cellSize; k++)
            {
                int xind = k + (i%map->nbCellSize) * cellSize;
                int yind = j + (i/map->nbCellSize) * cellSize;
                float proba = densityMap[yind*map->size + xind];
                float x = (xind + rand_float(0, 1)) * map->unitSize;
                float y = (yind + rand_float(0, 1)) * map->unitSize;

                if (bernoulli(proba))
                {
                    new_tree(map, map->cells[i], treeTemplate, x, y, proba < 0.2 ? 0.2 : proba);
                }
            }
        }

        printf("\r%d%%", (100*i)/(map->nbCellSize * map->nbCellSize));
        fflush(stdout);
    }

    printf("\nDone\n");

    /*renderable_free(treeTemplate);*/

    free(densityMap);
}
