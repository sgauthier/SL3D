#include "../renderable.h"
#include "../resources.h"

static float verticesX[] =
{
    0.0,  0.1,  0.0,   1.0, 0.0, 0.0,   0.0,  0.0,  0.1,
    0.0,  0.0,  0.1,   1.0, 0.0, 0.0,   0.0, -0.1,  0.0,
    0.0, -0.1,  0.0,   1.0, 0.0, 0.0,   0.0,  0.0, -0.1,
    0.0,  0.0, -0.1,   1.0, 0.0, 0.0,   0.0,  0.1,  0.0
};

static float verticesY[] =
{
     0.1, 0.0,  0.0,   0.0, 1.0, 0.0,    0.0, 0.0,  0.1,
     0.0, 0.0,  0.1,   0.0, 1.0, 0.0,   -0.1, 0.0,  0.0,
    -0.1, 0.0,  0.0,   0.0, 1.0, 0.0,    0.0, 0.0, -0.1,
     0.0, 0.0, -0.1,   0.0, 1.0, 0.0,    0.1, 0.0,  0.0
};

static float verticesZ[] =
{
     0.0,  0.1, 0.0,   0.0, 0.0, 1.0,    0.1,  0.0, 0.0,
     0.1,  0.0, 0.0,   0.0, 0.0, 1.0,    0.0, -0.1, 0.0,
     0.0, -0.1, 0.0,   0.0, 0.0, 1.0,   -0.1,  0.0, 0.0,
    -0.1,  0.0, 0.0,   0.0, 0.0, 1.0,    0.0,  0.1, 0.0
};

Renderable* frame_new(void)
{
    Renderable* axisX = renderable_new(12, verticesX, NULL, NULL, shaders[SHADER_SOLID_COLOR], 0);
    Renderable* axisY = renderable_new(12, verticesY, NULL, NULL, shaders[SHADER_SOLID_COLOR], 0);
    Renderable* axisZ = renderable_new(12, verticesZ, NULL, NULL, shaders[SHADER_SOLID_COLOR], 0);
    Renderable* frame = renderable_new(0, NULL, NULL, NULL, 0, 0);
    renderable_set_solid_color(axisX, 1.0, 0.0, 0.0);
    renderable_set_solid_color(axisY, 0.0, 1.0, 0.0);
    renderable_set_solid_color(axisZ, 0.0, 0.0, 1.0);
    renderable_add_child(frame, axisX);
    renderable_add_child(frame, axisY);
    renderable_add_child(frame, axisZ);
    return frame;
}
