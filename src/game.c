#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <unistd.h>

#include "game.h"
#include "viewer.h"
#include "resources.h"
#include "renderables/cube.h"
#include "renderables/frame.h"
#include "renderables/mesh.h"
#include "renderables/minimap.h"
#include "map.h"
#include "sled.h"
#include "forest.h"
#include "tree.h"
#include "dog.h"
#include "flag.h"
#include "quaternion.h"

#define NB_DOGS 3

struct Game
{
    Viewer* viewer;
    int running;
    double lastTime;

    struct Map* map;
    struct Sled* sled;
    Renderable* skybox;
    Dog* dog[NB_DOGS];
    Renderable* mapSledPoint;
};

static void cursor_callback(double xpos, double ypos, double dx, double dy, int buttonLeft, int buttonMiddle, int buttonRight, void* userData)
{
    struct Game* game = userData;
    struct Camera* camera = viewer_get_camera(game->viewer);
    struct Model* sledModel = sled_get_model(game->sled);
    Vec4 u = {0, 0, 0, 1}, w = {0, 1, 0, 0};
    Quaternion a, b, c;

    mul4mv(u, sledModel->matrix, w);
    quaternion_set_axis_angle(a, u, -dx / viewer_get_width(game->viewer));
    camera_get_right(camera, u);
    quaternion_set_axis_angle(b, u, -dy / viewer_get_height(game->viewer));
    quaternion_mul(c, a, camera->orientation);
    quaternion_mul(camera->orientation, b, c);

    camera_update_view(camera);
}

static void wheel_callback(double xoffset, double yoffset, void* userData)
{
}

static void key_callback(int key, int scancode, int action, int mods, void* userData)
{
    Vec3 t = {0, 0, 0};
    struct Game* game = userData;
    int i;

    if (action == GLFW_RELEASE && key == GLFW_KEY_SPACE)
        game->sled->brake = 0.01;

    if (action != GLFW_PRESS && action != GLFW_REPEAT)
        return;

    switch (key)
    {
        case GLFW_KEY_ESCAPE:
            game->running = 0;
            break;
        case GLFW_KEY_LEFT:
        case GLFW_KEY_A:
            dog_rotate(game->dog[NB_DOGS - 1], 0.1);
            break;
        case GLFW_KEY_RIGHT:
        case GLFW_KEY_D:
            dog_rotate(game->dog[NB_DOGS - 1], -0.1);
            break;
        case GLFW_KEY_DOWN:
        case GLFW_KEY_S:
            for (i = 0; i < NB_DOGS; i++)
                dog_decr_force(game->dog[i]);
            break;
        case GLFW_KEY_UP:
        case GLFW_KEY_W:
            for (i = 0; i < NB_DOGS; i++)
                dog_incr_force(game->dog[i]);
            break;
        case GLFW_KEY_SPACE:
            game->sled->brake = 0.2;
            break;
    }
}

static void close_callback(void* userData)
{
    struct Game* game = userData;
    game->running = 0;
}

static int init(struct Game* game, struct Params* params)
{
    GLuint stdShader;
    Renderable* flag;
    Vec3 t;
    int i;
    enum PullType pullType = PULL_SLED;
    void* previous;

    struct DirectionnalLight dl;
    dl.direction[0] = 1.0;
    dl.direction[1] = -1.0;
    dl.direction[2] = -1.0;
    dl.ambient[0] = 0.25;
    dl.ambient[1] = 0.25;
    dl.ambient[2] = 0.32;
    dl.diffuse[0] = 0.5;
    dl.diffuse[1] = 0.5;
    dl.diffuse[2] = 0.5;
    dl.specular[0] = 0.0;
    dl.specular[1] = 0.0;
    dl.specular[2] = 0.0;
    viewer_add_directionnal_light(game->viewer, &dl);

    if (params->fog)
    {
        game->skybox = cube_new(1000, shaders[SHADER_SOLID_TEXTURE], 0);
        stdShader = shaders[SHADER_FOG];
    }
    else
    {
        game->skybox = cube_new(1000, shaders[SHADER_SOLID_TEXTURE], textures[TEXTURE_SKYBOX]);
        stdShader = shaders[SHADER_BASIC];
    }

    renderable_set_solid_color(game->skybox, 0.7, 0.7, 0.8);
    renderable_toggle_always_draw(game->skybox);
    viewer_add_renderable(game->viewer, game->skybox);

    game->map = map_new(1024, 16, 1.5, params->terrainRoughness, stdShader);
    forest_generate(game->map, params->forestDensity, params->forestCompactness, 64, stdShader);
    viewer_add_renderable(game->viewer, game->map->master);

    flag = flag_new(game->map, stdShader);
    minimap_move_point(game->map, minimap_add_point(game->map->minimap, 0.43, 0.63, 0.14), renderable_get_model(flag)->position[0], renderable_get_model(flag)->position[2]);
    viewer_add_renderable(game->viewer, flag);

    previous = game->sled = sled_new();
    t[0] = 256.0 * 1.5 / 2.0;
    t[2] = 256.0 * 1.5 / 2.0;
    t[1] = map_get_height(game->map, t[0], t[2]);
    game->mapSledPoint = minimap_add_point(game->map->minimap, 0.4, 0.2, 0.0);
    renderable_move(game->sled->master, t);
    viewer_add_renderable(game->viewer, game->sled->master);

    for (i = 0; i < NB_DOGS; i++)
    {
        previous = game->dog[i] = dog_new(pullType, previous);
        pullType = PULL_DOG;
        t[0] += 3;
        t[1] = map_get_height(game->map, t[0], t[2]);
        renderable_move(dog_get_renderable(game->dog[i]), t);
        viewer_add_renderable(game->viewer, dog_get_renderable(game->dog[i]));
    }

    t[0] = 256.0 * 1.5 / 2.0 - 3.0;
    t[1] = map_get_height(game->map, t[0], t[2]) + 5.0;
    camera_move(viewer_get_camera(game->viewer), t);
    t[1] = 1; t[0] = t[2] = 0;
    camera_rotate(viewer_get_camera(game->viewer), t, -M_PI / 2.0);

    return game->map && game->sled;
}

struct Game* game_new(struct Params* params)
{
    struct Game* game;

    if ((game = calloc(1, sizeof(struct Game))))
    {
        game->running = 0;
        if ((game->viewer = viewer_new(1024, 768, "SL3D")))
        {
            viewer_set_callbacks(game->viewer, cursor_callback, wheel_callback, key_callback, close_callback, game);
            if (resources_init())
            {
                if (init(game, params))
                {
                    game->running = 1;
                    game->lastTime = glfwGetTime();
                    return game;
                }
            }
        }
    }

    game_free(game);
    return NULL;
}

void game_free(struct Game* game)
{
    int i;

    if (game)
    {
        renderable_free(game->skybox);
        sled_free(game->sled);
        map_free(game->map);
        viewer_free(game->viewer);
        for (i = 0; i < NB_DOGS; i++)
            dog_free(game->dog[i]);
        free(game);
    }
}

int game_is_running(struct Game* game)
{
    return game->running;
}

void game_over(struct Game* game, const char* message)
{
    printf("\x1b[31;1mGame over:\x1b[0;31m %s\x1b[0m\n", message); 
    game->running = 0;
}

void game_update(struct Game* game)
{
    Quaternion rot, swing, twist;
    Vec4 u = {-2, 1.75, 0, 1}, v, x = {1, 0, 0, 0}, y = {0, 1, 0, 0}, up, axis;
    struct Camera* camera = viewer_get_camera(game->viewer);
    double current = glfwGetTime(), dt = current - game->lastTime;
    float angle;
    
    game->lastTime = current;
    viewer_process_events(game->viewer);
    usleep(10 * 1000);

    switch (dog_update(game->dog[NB_DOGS - 1], game->map, dt))
    {
        case DOG_NO_ERROR:
            break;
        case DOG_SLED_OUT_OF_MAP:
            game_over(game, "you sled got lost in the universe, and you as well, and you died.");
            break;
        case DOG_SLED_TOO_MUCH_ROTATION:
            game_over(game, "your sled rotated so much that you fell and died.");
            break;
        case DOG_SLED_COLLISION_WITH_TREE:
            game_over(game, "your sled crashed onto a tree, and you died.");
            break;
        case DOG_OUT_OF_MAP:
            game_over(game, "a dog got lost in the universe, the sled and you as well, and you died.");
            break;
        case DOG_COLLISION_WITH_TREE:
            game_over(game, "a dog crashed onto a tree, the sled and you as well, and you died.");
            break;
        case DOG_COLLISION_WITH_SLED:
            game_over(game, "your sled killed a dog, so the others attacked you and you died.");
            break;
    }

    mul4mv(v, sled_get_model(game->sled)->matrix, u);
    memcpy(camera->position, v, sizeof(Vec3));
    camera_update_view(camera);
    mul4mv(v, sled_get_model(game->sled)->matrix, y);
    camera_get_up(camera, up);
    compute_rotation(up, v, axis, &angle);
    quaternion_set_axis_angle(rot, axis, angle);
    mul4mv(v, sled_get_model(game->sled)->matrix, x);
    quaternion_decompose_swing_twist(rot, v, swing, twist);
    quaternion_get_axis(twist, axis);
    camera_rotate(camera, axis, quaternion_get_angle(twist));

    minimap_move_point(game->map, game->mapSledPoint, camera->position[0], camera->position[2]);
    
    viewer_render(game->viewer);
}
